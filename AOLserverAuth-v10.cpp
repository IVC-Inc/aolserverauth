#include <stdio.h>
#include <node.h>
#include <v8.h>
#include "AOLsha.h"

using namespace std;
using namespace v8;

Handle<Value> doSHA1(const Arguments& args)
{
   HandleScope scope;
   int numArgs = args.Length();

   if(numArgs == 0) return scope.Close(String::New(""));

   Local<String> arg0 = Local<String>::Cast(args[0]);
   String::AsciiValue plainText_av(arg0);

   if(plainText_av.length() > 0) 
   {
      string plainText_s(*plainText_av);
      string cipherText = AOLsha(plainText_s);

      return scope.Close(String::New(cipherText.c_str()));
   }
   return scope.Close(String::New(""));
}

Handle<Value> doAuthenticate(const Arguments& args)
{
   HandleScope scope;

   if (args.Length() < 3)
   {
      ThrowException(Exception::TypeError(String::New("require password,salt,hash")));
      return scope.Close(Undefined());
   }

   Local<String> arg0 = Local<String>::Cast(args[0]);
   Local<String> arg1 = Local<String>::Cast(args[1]);
   Local<String> arg2 = Local<String>::Cast(args[2]);
   String::AsciiValue password_av(arg0);
   String::AsciiValue salt_av(arg1);
   String::AsciiValue hash_av(arg2);
   string password_s(*password_av);
   string salt_s(*salt_av);
   string hash_s(*hash_av);
   string cipherText = AOLsha(password_s+salt_s);

   if(cipherText == hash_s) return scope.Close(Boolean::New(true));
   return scope.Close(Boolean::New(false));
}

void init(Handle<Object> exports)
{
  exports->Set(String::NewSymbol("sha1"),FunctionTemplate::New(doSHA1)->GetFunction());
  exports->Set(String::NewSymbol("authenticate"),FunctionTemplate::New(doAuthenticate)->GetFunction());
}

NODE_MODULE(AOLserverAuth,init)
