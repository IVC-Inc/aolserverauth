#include <stdio.h>
#include <node.h>
#include <v8.h>
#include "AOLsha.h"

using namespace std;
using namespace v8;

void doSHA1(const FunctionCallbackInfo<Value> &args)
{
   Isolate *I = Isolate::GetCurrent();
   HandleScope scope(I);
   int numArgs = args.Length();

   if(numArgs == 0) 
   {
      args.GetReturnValue().Set(String::NewFromUtf8(I,""));
      return;
   }

   Local<String> arg0 = args[0]->ToString();
   String::Utf8Value plainText_av(arg0);

   if(plainText_av.length() > 0) 
   {
      string plainText_s(*plainText_av);
      string cipherText = AOLsha(plainText_s);

      args.GetReturnValue().Set(String::NewFromUtf8(I,cipherText.c_str()));
   }
   else args.GetReturnValue().Set(String::NewFromUtf8(I,""));
}

void doAuthenticate(const FunctionCallbackInfo<Value> &args)
{
   Isolate *I = Isolate::GetCurrent();
   HandleScope scope(I);

   if (args.Length() < 3)
   {
      I->ThrowException(Exception::TypeError(String::NewFromUtf8(I,"require password,salt,hash")));
      args.GetReturnValue().Set(Undefined(I));
      return;
   }

   Local<String> arg0 = Local<String>::Cast(args[0]);
   Local<String> arg1 = Local<String>::Cast(args[1]);
   Local<String> arg2 = Local<String>::Cast(args[2]);
   String::Utf8Value password_av(arg0);
   String::Utf8Value salt_av(arg1);
   String::Utf8Value hash_av(arg2);
   string password_s(*password_av);
   string salt_s(*salt_av);
   string hash_s(*hash_av);
   string cipherText = AOLsha(password_s+salt_s);

   if(cipherText == hash_s) args.GetReturnValue().Set(Boolean::New(I,true));
   else  args.GetReturnValue().Set(Boolean::New(I,false));
}

void init(Handle<Object> exports)
{
  NODE_SET_METHOD(exports,"sha1",doSHA1);
  NODE_SET_METHOD(exports,"authenticate",doAuthenticate);
}

NODE_MODULE(AOLserverAuth,init)
