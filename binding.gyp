{
  "targets":
  [
    {
      "variables":
      {
         "version":"<!(node --version | sed -e 's/^v\([0-9]*\.[0-9]*\).*$/\\1.x/')"
      },
      "target_name": "AOLserverAuth",
      "sources": [ "AOLsha.cpp" ],
      "conditions":
      [
        [ "version=='0.10.x'",
          {
             "sources": [ "AOLserverAuth-v10.cpp" ]
          },
          {
             "sources": [ "AOLserverAuth-v11.cpp" ]
          }
        ]
      ]
    }
  ]
}
